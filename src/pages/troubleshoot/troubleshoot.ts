import { OnInit, Component } from '@angular/core';
// import { HttpService} from '../../services/http.service';

import { NavController } from 'ionic-angular';
import{ Troubleshoot_2Page } from '../troubleshoot-2/troubleshoot-2';
// import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { FormGroup, FormControl } from '@angular/forms';

import { ProductListProvider } from '../../providers/product-list/product-list';
import { DownloadPage} from './../download/download';
import { Platform } from 'ionic-angular';
import {HomePage } from './../home/home';
import { RegisterPage } from './../register/register';
import * as $ from "jquery";
// declare var jquery:any;
declare var $ :any;
@Component({
    selector: 'page-troubleshoot',
    templateUrl: 'troubleshoot.html'
  })
  export class TroubleshootPage implements OnInit {

    surveyForm: FormGroup;
    mySurvey: any;
    mySurveyChildOne: any;
    survey:any;

    feedbackoverlayHidden: boolean = true;
    feedbackFormoverlayHidden: boolean = true;

    deviceIs: any;
    data: any;
    tabVisibl: any;
    all: any;

    show: any;
    showSib: any;

    backAllBtn: any;

    Printer: any;
    Hardware: any;
    Software: any;
    Mobile: any;
    Troubleshoot: any;
    actTabClass: any;
    childA: Array<{cat: string, txt: string}>;

    siblingStatus = 'PrinterList_2_all';

    productLevel1List: Array<{title: string, ref_id: string}>;
    productLevel2List: Array<{title: string}>;

    bannerPics: Array<{title: string, modelPic: string}>;

    currntComp: any;

    menuBackBtn: Boolean = true;

    langList: Array<{ lgName: string, lgID: number, flag:string }>;
    lang: any;
    currentCountry;
    countryClass = sessionStorage.getItem('langClass');
    countryName: any;

        constructor(
            private productListProvider:ProductListProvider,
            public navCtrl: NavController,
            public plt: Platform,
        ) {}

    ngOnInit(): void {

      this.langList = [{ lgName: 'United States (English)', lgID: 1, flag: 'us' },
      { lgName: 'Canada (French)', lgID: 109, flag: 'canada' },
      { lgName: 'United States (Spanish)', lgID: 111, flag: 'us' },
      { lgName: 'Canada (English)', lgID: 110, flag: 'canada' },
      { lgName: 'United Kingdom (English)', lgID: 108, flag: 'uk' }];


      if (sessionStorage.getItem("lang") == null && sessionStorage.getItem("lang") == "") {
        this.lang = 1;
        this.countryName = 1;
        sessionStorage.setItem('lang', '1');
      }
      else {
        this.lang = sessionStorage.getItem('lang');
        this.countryName = sessionStorage.getItem('lang');
        console.log(this.countryName);
      }

      let view = this.navCtrl.getActive();
      this.currntComp = view.component.name;

      this.platform();


      this.tabVisibl = false;
      // this.tabVisibl = 'guid2';
      this.all = true;
      this.backAllBtn = false;

      this.productLevel1List = [];
      this.productLevel2List = [];
      this.bannerPics = [];

      this.onSubmit();

      this.GetHeadings();

      this.productBanner();

      // this.feedbackFormOpen();

// this.siblingStatus = 'PrinterList_2_all';

    }

    selectOptions: any = {
      title: 'Language',
      mode: 'md',
    };

    setLang(val: any) {
      console.log(val);
      sessionStorage.setItem('lang', val);
      // this.navCtrl.setRoot(this.navCtrl.getActive().component);
      this.navCtrl.push(this.navCtrl.getActive().component);


      this.currentCountry = this.langList.filter(c => {
        if (c.lgID == val) {
          return c;
        }
      });
      console.log(this.currentCountry[0].flag);
      this.countryClass = this.currentCountry[0].flag;
      sessionStorage.setItem('langClass', this.currentCountry[0].flag);
    }

    feedbackFormOpen(){
      this.feedbackFormoverlayHidden = false;
    }

    yesThankYou(){
      this.feedbackoverlayHidden = true;
    }

    platform(){
      if (this.plt.is('iphone')) {
        this.deviceIs = 'iphone';
      }
      else if(this.plt.is('android'))
      {
        this.deviceIs = 'Android';

      }
    }

    form = new FormGroup({
      survey: new FormControl(),
      mySurveyChildOne: new FormControl(),
    });

    putSurvey(){
      // var data = this.form.value;
      // console.log(this.mySurvey);
      // console.log(this.mySurveyChildOne);

      var postdata = {
        channel: this.mySurveyChildOne,
        feedback_id: this.mySurvey,
        num_id: localStorage.getItem('prod_num_id'),
        third_level:'',
        page_name: "Troubleshooting",
        device_id: this.deviceIs,
      }
      console.log(postdata);
      $.ajax({
        url: 'http://124.30.44.230/funaihelpver1/api/ver1/survey',
        method:'POST',
        crossDomain: true,
        beforeSend:function(xhr){
          xhr.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
        },
        async:true,
        data:postdata,
        success: function(result) {
          console.log(result);
          $('.my-overlay').hide();
        },
        error: function(request,msg,error) {
        }
      });
    }


    subOpt(event)
    {
      // this.childA = [
      //   {cat : 'Troubleshoot', txt : 'Paper'},
      //   {cat : 'Troubleshoot', txt : 'Other errors'},
      //   {cat : 'Troubleshoot', txt : 'Black Ink'},
      //   {cat : 'Troubleshoot', txt : 'Black and Color Ink'},
      //   {cat : 'Troubleshoot', txt : 'Color ink'}
      // ]

      // this.childA.push(
      //   {cat : 'Troubleshoot', txt : 'Paper'},
      //   {cat : 'Troubleshoot', txt : 'Other errors'},
      //   {cat : 'Troubleshoot', txt : 'Black Ink'},
      //   {cat : 'Troubleshoot', txt : 'Black and Color Ink'},
      //   {cat : 'Troubleshoot', txt : 'Color ink'}
      // );

      console.log(event);

      if(event.srcElement.className == 'img-circle')
      {
        this.actTabClass = event.srcElement.parentElement.className;
      }
      else
      {
        this.actTabClass = event.srcElement.className;
      }

      if(this.actTabClass == 'Printer')
      {
      this.Printer = true;
        this.Software = false;
        this.Hardware = false;
        this.Mobile = false;
        this.Troubleshoot = false;
      }
      else if(this.actTabClass == 'Hardware')
      {
      this.Hardware = true;
        this.Software = false;
        this.Printer = false;
        this.Mobile = false;
        this.Troubleshoot = false;
      }
      else if(this.actTabClass == 'Software')
      {
        this.Software = true;
        this.Hardware = false;
        this.Printer = false;
        this.Mobile = false;
        this.Troubleshoot = false;
      }
      else if(this.actTabClass == 'Mobile')
      {
        this.Mobile = true;
        this.Hardware = false;
        this.Printer = false;
        this.Software = false;
        this.Troubleshoot = false;
      }
      // else{
      //   this.backAllBtn = false;
      // }
      else if(this.actTabClass == 'Troubleshoot')
      {
        this.Troubleshoot = true;
        this.Hardware = false;
        this.Printer = false;
        this.Mobile = false;
        this.Software = false;
      }


      if(this.Printer == true || this.Hardware == true || this.Software == true || this.Mobile == true || this.Troubleshoot == true)
      {
        this.all = false;
        this.backAllBtn = true;
      }
      else
      {
        this.all = true;
        this.backAllBtn = false;
      }

    }

    backAll()
    {
      this.all = true;

      this.Printer = false;
      this.Hardware = false;
      this.Software = false;
      this.Mobile = false;
      this.Troubleshoot = false;
      this.backAllBtn = false;
      // this.Printer, this.Hardware, this.Software, this.Mobile, this.Troubleshoot, this.backAllBtn = false;
    }

    onSubmit() {

      // var tokenId = localStorage.getItem('token_array');
      // var tokenId = this.navParams.get('token_array');

      console.log(localStorage.getItem('prod_num_id'));
// http://124.30.44.230/funaihelpver1/api/ver1/secondlevel.json?firstlevel=1&lang=1
      // return this.httpService.get('thirdlevel.json?secondlevel=2&lang=1').then(
      //   (success) => {
          this.productListProvider.getProducts('thirdlevel.json?secondlevel=2&lang=1')  .subscribe(success => {

          if (success) {
            console.log(success);
            // this.android_url = success.android_playstore_url;
            // this.ownersManual = success.downloads[1].owners;
            // this.quickstartGuide = success.downloads[1].quickstart;
            // this.GoogleCloud = success.google_cloud_url;
            // this.AirPrintUrl = success.about_print_url;
            // this.WarrantySafety = success.downloads[1].warranty_safety_sheet;
            for (var i = 0; i < success.length; i++) {
console.log( success[i]);
              this.productLevel2List.push({ title: success[i].level_details });
            }
          } else {
            console.log('Error in success');
          }
        });
    }

    GetHeadings() {

      // var tokenId = localStorage.getItem('token_array');
      // var tokenId = this.navParams.get('token_array');

      console.log(localStorage.getItem('prod_num_id'));

      console.log(this.lang);

      // return this.httpService.get('firstlevel.json?prodid=1&lang=' + this.lang ).then(
      //   (success) => {
      this.productListProvider.getProducts('firstlevel.json?prodid=1&lang=' + this.lang )  .subscribe(success => {

          if (success) {
console.log(success.records[2].level_details);
            for (var i = 0; i < success.total_pages; i++) {

              this.productLevel1List.push({ title: success.records[i].level_details, ref_id: success.records[i].ref_id });
            }
            console.log(this.productLevel1List);
          } else {
            console.log('Error in success');
          }
        });
    }

    onclick(new_title, ref_id){
    this.navCtrl.push(Troubleshoot_2Page,{'new_title': new_title, 'ref_id': ref_id});
}


productBanner() {

  var tokenId = localStorage.getItem('token_array');
  // var tokenId = this.navParams.get('token_array');
  console.log(tokenId);

  this.productListProvider.getProducts('productlist.json?compid=1&prodid=1&lang=1')  .subscribe(success => {
  // return this.httpService.get('productlist.json?compid=1&prodid=1&lang=1').then(
  //   (success) => {
      if (success) {
        this.bannerPics.push({ title: success.model_info[tokenId].model_number, modelPic: success.model_info[tokenId].img });
      } else {
        console.log('Error in success');
      }
    })
  }

  gotoDownld()
  {
    this.navCtrl.push(DownloadPage);
  }

  pushHome() {
    this.navCtrl.setRoot(HomePage);
  }

  gotoBack()
  {
    this.navCtrl.pop();
  }
  pushRegister() {
     this.navCtrl.setRoot(RegisterPage);
  }

  }
