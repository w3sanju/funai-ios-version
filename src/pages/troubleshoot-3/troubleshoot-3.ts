import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
// import { HttpService} from '../../services/http.service';
import{ FaqsPage } from '../faqs/faqs';

 import { ProductListProvider } from '../../providers/product-list/product-list';

 import { TabsPage } from '../tabs/tabs';
 import { DownloadPage} from './../download/download';
 import {HomePage } from './../home/home';
 import { RegisterPage } from './../register/register';

@Component({
  selector: 'page-troubleshoot-3',
  templateUrl: 'troubleshoot-3.html',
})
export class Troubleshoot_3Page {
  new_title: string = this.navParams.get('new_title');
  productLevel2List: Array<{title: string, New_id3: string}>;
  // productLevel4List: Array<{title: string, status: Number}>;
  title_3: string;
  productLevel4ListTitle: any = ['faqstatus','videostatus','vchatstatus','driverstatus','achatstatus','callstatus'];
  faqstatus: string;
  videostatus: string;
  vchatstatus: string;
  driverstatus: string;
  achatstatus: string;
  callstatus: string;
  productLevel3List: Boolean = true;
  productLevel4List: Boolean = false;

  tabsPage = TabsPage;

  langList: Array<{ lgName: string, lgID: number, flag:string }>;
  lang: any;
  currentCountry;
  countryClass = sessionStorage.getItem('langClass');
  countryName: any;

  bannerPics: Array<{title: string, modelPic: string}>;
  menuBackBtn: Boolean = true;

  constructor(
    // private httpService: HttpService,
    private productListProvider:ProductListProvider,
    public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Troubleshoot_3Page');
    console.log(this.navCtrl.getActive().name);
  }

  ngOnInit(): void {

    this.productLevel2List = [];
    // this.productLevel4List = [];

    this.langList = [{ lgName: 'United States (English)', lgID: 1, flag: 'us' },
    { lgName: 'Canada (French)', lgID: 109, flag: 'canada' },
    { lgName: 'United States (Spanish)', lgID: 111, flag: 'us' },
    { lgName: 'Canada (English)', lgID: 110, flag: 'canada' },
    { lgName: 'United Kingdom (English)', lgID: 108, flag: 'uk' }];


    if (sessionStorage.getItem("lang") == null && sessionStorage.getItem("lang") == "") {
      this.lang = 1;
      this.countryName = 1;
      sessionStorage.setItem('lang', '1');
    }
    else {
      this.lang = sessionStorage.getItem('lang');
      this.countryName = sessionStorage.getItem('lang');
      console.log(this.countryName);
    }

    this.bannerPics = [];
    this.productBanner();


    this.onSubmit();
    // this.subLevel4();
  }

  selectOptions: any = {
    title: 'Language',
    mode: 'md',
  };
  // http://124.30.44.230/funaihelpver1/api/ver1/secondlevel.json?firstlevel=4&lang=1

  setLang(val: any) {
    console.log(val);
    sessionStorage.setItem('lang', val);
    // this.navCtrl.setRoot(this.navCtrl.getActive().component);


    this.currentCountry = this.langList.filter(c => {
      if (c.lgID == val) {
        return c;
      }
    });
    console.log(this.currentCountry[0].flag);
    this.countryClass = this.currentCountry[0].flag;
    sessionStorage.setItem('langClass', this.currentCountry[0].flag);
  }

  onSubmit() {

    this.productListProvider.getProducts('thirdlevel.json?secondlevel='+this.navParams.get('level_id3')+'&lang=1')  .subscribe(success => {

  // return this.httpService.get('thirdlevel.json?secondlevel='+this.navParams.get('level_id3')+'&lang=1').then(

    // return this.httpService.get('downloads.json?lang=1&numid='+localStorage.getItem('prod_num_id')).then(
      // (success) => {
        // console.log(success.model_number);
        if (success) {
          console.log(success);
          if(success.length >= 1)
          {
            for (var i = 0; i < success.length; i++) {
              this.productLevel2List.push({ title: success[i].level_details, New_id3: success[i].level_id });
              console.log(' level_id_3 is ' + success[i].level_id);
            }
          }
          else{
            this.productLevel2List.push({ title: 'No record found', New_id3: 'No id' });
          }

  console.log(this.productLevel2List);
          // this.productPics.push({ title: success.model_info[tokenId].model_number, modelPic: success.model_info[tokenId].img });
        } else {
          console.log('Error in success');
        }
      });
  }


  subLevel4(level_id3New) {

console.log(level_id3New);
localStorage.setItem('level_id3New', level_id3New);
this.productListProvider.getProducts('smartchannels.json?third_level='+level_id3New+'&num_id='+localStorage.getItem('prod_num_id')+'&lang='+sessionStorage.getItem('lang'))  .subscribe(success => {

  // return this.httpService.get('smartchannels.json?third_level='+level_id3New+'&num_id='+localStorage.getItem('prod_num_id')+'&lang='+sessionStorage.getItem('lang')).then(

    // return this.httpService.get('downloads.json?lang=1&numid='+localStorage.getItem('prod_num_id')).then(
      // (success) => {
        // console.log(success.model_number);
        if (success) {
          this.faqstatus = success[0]['faqstatus'];
          this.videostatus = success[0]['videostatus'];
          this.vchatstatus = success[0]['vchatstatus'];
          this.driverstatus = success[0]['driverstatus'];
          this.achatstatus = success[0]['achatstatus'];
          this.callstatus = success[0]['callstatus'];

          if(success.length >= 1)
          {
            // for (var i = 0; i < 5; i++) {
            //   console.log(this.productLevel4ListTitle[i]);
            //   this.productLevel4List.push({ title: this.productLevel4ListTitle[i], status: success[i][this.productLevel4ListTitle[i]] });
            // }
          }
          else{
            // this.productLevel4List.push({ title: 'No record found', status: 0 });
          }

  console.log(this.productLevel4List);
          // this.productPics.push({ title: success.model_info[tokenId].model_number, modelPic: success.model_info[tokenId].img });
        } else {
          console.log('Error in success');
        }
      });
  }

  level_id4(title_3, level_id3New) {
      this.title_3 = title_3;
      this.productLevel4List = true;
    this.subLevel4(level_id3New);
  }

  levelTo5() {
      this.navCtrl.push(FaqsPage,{'level_id3': this.navParams.get('new_id')});
  }

  gotTabPage(tabValue) {
    if (tabValue == 0) {
      localStorage.setItem('activeTab', tabValue);
      this.navCtrl.push(this.tabsPage,{'hideHeader': true});
      console.log(tabValue);
    } else {
      localStorage.setItem('activeTab', tabValue);
      this.navCtrl.push(this.tabsPage,{'hideHeader': true});
      console.log(tabValue);
    }
  }

  productBanner() {

    var tokenId = localStorage.getItem('token_array');
    // var tokenId = this.navParams.get('token_array');
    console.log(tokenId);

    this.productListProvider.getProducts('productlist.json?compid=1&prodid=1&lang=1')  .subscribe(success => {
    // return this.httpService.get('productlist.json?compid=1&prodid=1&lang=1').then(
    //   (success) => {
        if (success) {
          this.bannerPics.push({ title: success.model_info[tokenId].model_number, modelPic: success.model_info[tokenId].img });
        } else {
          console.log('Error in success');
        }
      })
    }

  gotoDownld()
  {
    this.navCtrl.push(DownloadPage);
  }

  pushHome() {
    this.navCtrl.setRoot(HomePage);
  }

  pushBack() {
    this.navCtrl.pop();
  }
  pushRegister() {
     this.navCtrl.setRoot(RegisterPage);
  }

}
