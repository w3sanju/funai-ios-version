import 'rxjs/add/operator/switchMap';
import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
// import { Headers, RequestOptions } from '@angular/http';
import { FormGroup, FormControl, Validators, FormBuilder, } from '@angular/forms';
import { Platform } from 'ionic-angular';
// import { HttpService} from '../../services/http.service';
import { AlertController } from 'ionic-angular';
import { ProductListProvider } from '../../providers/product-list/product-list';
import { TroubleshootPage} from './../troubleshoot/troubleshoot'
import {HomePage } from './../home/home';
import { RegisterPage } from './../register/register';

import * as $ from "jquery";
// declare var jquery:any;
declare var $ :any;

@Component({
    selector: 'page-download',
    templateUrl: 'download.html'
  })
  export class DownloadPage implements OnInit {

    countryName: any;
    header_status: any;



    registerForm: FormGroup;
    firstName: any;
    // lastName: any;
    emailAddress: any;
    F_os_name: any;
    F_description: any;
    F_version_name: any;
    F_download_type: any;
    sucessMessage : any;

    deviceIs: any;
    data: any;
    tabVisibl: any;
    all: any;


    supportPrinterHidden: boolean = true;

    show: any;
    showSib: any;

    backAllBtn: any;

    Drivers: any;
    Firmware: any;
    Manual: any;
    Connect: any;
    Download: any;
    downloadDriver: any;
    downloadFirmware: any;

    actTabClass: any;

    android_url: any;
    ios_url: any;
    ownersManual: any;
    quickstartGuide : any;
    GoogleCloud: string;
    AirPrintUrl: string;
    WarrantySafety: string;
    // OSTypes: any;

    // lang = sessionStorage.getItem('lang');
    downloadH1: string;
    downloadH2: string;
    downloadH3: string;
    downloadH4: string;
    downloadH5: string;
    downloadSub1: string;
    downloadSub2: string;
    downloadSub3: string;
    downloadSub4: string;
    downloadSub5: string;
    downloadSub6: string;
    downloadSub7: string;

    downloadName: any;
    myOs: any;
    os_name: any;
    description: any;
    version_name: any;
    download_type: any;

    agreeTC: Boolean;

    supported_deviceList: any;

    langList: Array<{ lgName: string, lgID: number, flag:string }>;
    getFile_path: Array<{ f_path: any, f_desc:string }> = [] ;


    currentCountry;
    countryClass = sessionStorage.getItem('langClass');

    menuBackBtn: Boolean = true;
    lang: any;


    constructor(
      private productListProvider:ProductListProvider,
      public alertCtrl: AlertController,
      // private _HttpService: HttpService,
      private fb: FormBuilder,
      public plt: Platform,
      // private httpService: HttpService,
      public navCtrl: NavController,
      public navParams: NavParams,
      public viewCtrl: ViewController
      // private router: Router
    )
    {
      this.viewCtrl = this.navParams.get('viewCtrl');
    }

    ionViewWillEnter() {
      // this.onSubmit();
      this.productPics = [];



    }

    ionViewCanLeave() {
      // alert('Exit from page');
      // this.showAlert();
    }

    productPics: Array<{title: string, modelPic: string}>;
    OSTypes: Array<{title: string, osId: string}>;
    downloadTypes: string;

    bannerPics: Array<{title: string, modelPic: string}>;

    modelPic: any;

    multiple_download_path: Array<{name: any, description: any}>;
    selectedProduct: string;


    ngOnInit(): void {
      this.fb;
this.getFile_path =[];
this.getFile_path.length = 0;
// this.getFile_path.splice(0,this.getFile_path.length);
      this.multiple_download_path = [];
      this.multiple_download_path.length = 0;
      this.multiple_download_path.splice(0,this.multiple_download_path.length);

      this.onSubmit();
      console.log(this.getFile_path);


      this.langList = [{ lgName: 'United States (English)', lgID: 1, flag: 'us' },
      { lgName: 'Canada (French)', lgID: 109, flag: 'canada' },
      { lgName: 'United States (Spanish)', lgID: 111, flag: 'us' },
      { lgName: 'Canada (English)', lgID: 110, flag: 'canada' },
      { lgName: 'United Kingdom (English)', lgID: 108, flag: 'uk' }];

      if (sessionStorage.getItem("lang") == null && sessionStorage.getItem("lang") == "") {
        this.lang = 1;
        this.countryName = 1;
        sessionStorage.setItem('lang', '1');
      }
      else {
        this.lang = sessionStorage.getItem('lang');
        this.countryName = sessionStorage.getItem('lang');
        console.log(this.countryName);
      }

      console.log(this.lang);

      // this.onSubmit();
      this.productPics = [];
      this.OSTypes = [];
      this.tabVisibl = false;
      // this.tabVisibl = 'guid2';
      this.all = true;
      this.backAllBtn = false;
      this.bannerPics = [];
      // this.getFile_path = [];

      if (this.plt.is('iphone')) {
        this.deviceIs = 'iphone';
        console.log('I am an iOS device!');
      }
      else if(this.plt.is('android'))
      {
        console.log('I am not iOS device!');
        this.deviceIs = 'Android';

      }

      // this.productBanner();
      this.modelPic = localStorage.getItem('bnrImg');

      this.getContent();
      this.getOsDetails();

      this.agreeTC = false;

      this.createFormControls();
      this.createForm();

      // console.log(localStorage.getItem('refPage'));
      if(localStorage.getItem("refPage") !== null && localStorage.getItem("refPage") !== "" && localStorage.getItem("refPage") !== "undefined")
      {
        this.header_status = false;
      }
      else
      {
        this.header_status = true;
      }


    }

    selectOptions: any = {
      title: 'Language',
      mode: 'md',
    };

    createForm() {
      this.registerForm = new FormGroup({
        firstName: this.firstName,
        emailAddress: this.emailAddress,
        // myOs: this.myOs
      });
    }

    createFormControls(){
      this.firstName = new FormControl(null, [Validators.required, Validators.pattern('[a-zA-Z][a-zA-Z ]+')]);
      this.emailAddress = new FormControl(null, [Validators.required, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')]);
    }

    SubmitDownloadMail(myOs,mydescription,myversion_name,mydownload_type,myfilePath,mydownloadTypes){

      // console.log(this.getFile_path);

      // for (var i = 0; i < this.getFile_path.length; i++) {
      //   if (i == 0) {
      //       this.multiple_download_path = "'http://www.funaihelp.com/home/file_download/media-uploads-" + mydownload_type +"-" + localStorage.getItem('prod_num_id') + "/" + this.getFile_path[i].f_path + "/" + this.getFile_path[i].f_path + "' ,";
      //   }
      //   else if (i != this.getFile_path.length-1) {
      //       this.multiple_download_path = this.multiple_download_path + "'http://www.funaihelp.com/home/file_download/media-uploads-" + mydownload_type +"-" + localStorage.getItem('prod_num_id') + "/" + this.getFile_path[i].f_path + "/" + this.getFile_path[i].f_path ;
      //   } else {
      //     // this.multiple_download_path = this.multiple_download_path + this.getFile_path[i].f_path;
      //     this.multiple_download_path = this.multiple_download_path + "' , 'http://www.funaihelp.com/home/file_download/media-uploads-" + mydownload_type +"-" + localStorage.getItem('prod_num_id') + "/" + this.getFile_path[i].f_path + "/" + this.getFile_path[i].f_path + "' ";
      //
      //   }
      //
      // }

      this.multiple_download_path = [];
      this.multiple_download_path.length = 0;
      this.multiple_download_path.splice(0,this.multiple_download_path.length);


      for (var i = 0; i < this.getFile_path.length; i++) {
          var aa = "http://www.funaihelp.com/home/file_download/media-uploads-" + mydownload_type +"-" + localStorage.getItem('prod_num_id') + "/" + this.getFile_path[i].f_path + "/" + this.getFile_path[i].f_path;

        this.multiple_download_path.push({name: aa , description: this.getFile_path[i].f_desc });

        // if (i == 0) {
        //   var aa = "http://www.funaihelp.com/home/file_download/media-uploads-" + mydownload_type +"-" + localStorage.getItem('prod_num_id') + "/" + this.getFile_path[i].f_path + "/" + this.getFile_path[i].f_path;
        //   this.multiple_download_path = [{name: aa , description:'Recommended Printer driver & utilities for Windows operating system Version 1.0.0.2'}] ;
        //
        //     // this.multiple_download_path = "{'name': 'http://www.funaihelp.com/home/file_download/media-uploads-" + mydownload_type +"-" + localStorage.getItem('prod_num_id') + "/" + this.getFile_path[i].f_path + "/" + this.getFile_path[i].f_path + "' , 'description':'Recommended Printer driver & utilities for Windows operating system Version 1.0.0.2'}," ;
        // }
        // else if (i != this.getFile_path.length-1) {
        //   this.multiple_download_path.push({name: aa , description:'Recommended Printer driver & utilities for Windows operating system Version 1.0.0.2'}) ;
        //
        //     // this.multiple_download_path = this.multiple_download_path + "{'name': 'http://www.funaihelp.com/home/file_download/media-uploads-" + mydownload_type +"-" + localStorage.getItem('prod_num_id') + "/" + this.getFile_path[i].f_path + "/" + this.getFile_path[i].f_path+ "' , 'description':'Recommended Printer driver & utilities for Windows operating system Version 1.0.0.2'}," ;
        // } else {
        //   // this.multiple_download_path = this.multiple_download_path + this.getFile_path[i].f_path;
        //
        //   this.multiple_download_path.push({name: aa , description:'Recommended Printer driver & utilities for Windows operating system Version 1.0.0.2'}) ;
        //
        //   // this.multiple_download_path = this.multiple_download_path + "{'name': 'http://www.funaihelp.com/home/file_download/media-uploads-" + mydownload_type +"-" + localStorage.getItem('prod_num_id') + "/" + this.getFile_path[i].f_path + "/" + this.getFile_path[i].f_path + "' "+ " , 'description':'Recommended Printer driver & utilities for Windows operating system Version 1.0.0.2'}";
        // }
      }

      const myObjStr = JSON.stringify(this.multiple_download_path);

      console.log(JSON.parse(myObjStr));

$('#sucessMessage').html('<h5>Loading.... </h5>');
      // $('body').css('background-color','blue');
      if (this.registerForm.valid) {
        var data = this.registerForm.value;
        this.registerForm.reset();
        // var file_path_full = "http://www.funaihelp.com/home/file_download/media-uploads-" + mydownload_type +'-' + localStorage.getItem('prod_num_id') + "/" + myfilePath + '/' + myfilePath;

        // var file_path_full = "http://www.funaihelp.com/home/file_download/media-uploads-" + mydownload_type +'-' + localStorage.getItem('prod_num_id') + "/" + this.getFile_path + '/' + this.getFile_path;


      var postdata = {
        os_name: myOs,
        version_name: myversion_name,
        description: mydescription,
        download_type: mydownload_type,
        model_name: this.selectedProduct,
        // file_path:[{'name':'http://124.30.44.230/funaihelpver1/home/file_download/media-uploads-drivers-7/KODAKVERITE-PREINSTALLER-v1.0.0.2-170413.exe/KODAKVERITE-PREINSTALLER-v1.0.0.2-170413.exe','description':'Recommended Printer driver & utilities for Windows operating system Version 1.0.0.2'},{'name':'http://124.30.44.230/funaihelpver1/home/file_download/media-uploads-drivers-7/KODAKVERITE-PREINSTALLER-v1.0.0.2-170413.exe/KODAKVERITE-PREINSTALLER-v1.0.0.2-170413.exe','description':'Recommended Printer driver & utilities for Windows operating system Version 1.0.0.2'}],
        file_path: this.multiple_download_path,
        // file_path: "http://www.funaihelp.com/home/file_download/media-uploads-drivers-7/KODAKVERITE-PREINSTALLER-v1.0.0.2-170413.exe/" + myfilePath,
        user_email: data.emailAddress,
        user_name: data.firstName
        };
        console.log(postdata);
       $.ajax({
          url: 'http://124.30.44.230/funaihelpver1/api/ver1/send_downloads',
          method:'POST',
          crossDomain: true,
          beforeSend:function(xhr){
            xhr.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
          },
          async:true,
          data:postdata,
          success: function(result) {
              console.log(result.message);
              // this.sucessMessage = "Send Mail successfully.";
              // $('#sucessMessage').fadeIn(100).text('Mail successfully sent!');
              $('#sucessMessage').html('<h5>Mail successfully sent! </h5>');
          },
          error: function(request,msg,error) {
              // this.sucessMessage = "Send Mail - Server Request Error !";
              // $('#sucessMessage').text("Send Mail - Server Request Error !");
              $('#sucessMessage').html('<h5>Send Mail - Server Request Error !</h5>');
              console.log('Error code: '+error);
              console.log('Error message: '+msg);
          }
      });

      this.multiple_download_path = [];
      this.multiple_download_path.length = 0;
      this.multiple_download_path.splice(0,this.multiple_download_path.length);

    }

}

    showAlert() {
      console.log('showAlert')
    const alert = this.alertCtrl.create({
      title: 'TERMS AND SERVICES',
      subTitle: 'FUNAI SOFTWARE LIMITED WARRANTY AND LICENSE AGREEMENT',
      message: 'License Agreement is a legal agreement between you (either an individual or a single entity) and Funai Electric Co., Ltd. and its subsidiary companies (individually and collectively \"Funai\") that, to the extent your Funai product or Software Program is not otherwise subject to a written software license agreement between you and Funai or its suppliers, governs your use of any Software Program installed on or provided by Funai for use in connection with your Funai product. The term \"Software Program\" includes machine-readable instructions, audio\/visual content (such as images and recordings), and associated media, printed materials and electronic documentation, whether incorporated into, distributed with or for use with your Funai product.\r\n\r\n\r\nSTATEMENT OF SOFTWARE LIMITED WARRANTY. Funai warrants that the media (e.g., DVD or compact disk) on which the Software Program (if any) is furnished is free from defects in materials and workmanship under normal use during the warranty period. The warranty period is ninety (90) days and commences on the date the Software Program is delivered to the original end-user. This limited warranty applies only to Software Program media purchased new from Funai or an Authorized Funai Reseller or Distributor. Funai will replace the Software Program should it be determined that the media does not conform to this limited warranty.\r\n\r\n\r\nDISCLAIMER AND LIMITATION OF WARRANTIES. EXCEPT AS PROVIDED IN THIS LICENSE AGREEMENT AND TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, FUNAI AND ITS SUPPLIERS PROVIDE THE SOFTWARE PROGRAM \"AS IS\" AND HEREBY DISCLAIM ALL OTHER WARRANTIES AND CONDITIONS, EITHER EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, TITLE, NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE, AND ABSENCE OF VIRUSES, ALL WITH REGARD TO THE SOFTWARE PROGRAM. TO THE EXTENT FUNAI CANNOT BY LAW DISCLAIM ANY COMPONENT OF THE IMPLIED WARRANTIES OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, FUNAI LIMITS THE DURATION OF SUCH WARRANTIES TO THE 90-DAY TERM OF THE EXPRESS SOFTWARE LIMITED WARRANTY\r\n\r\n\r\nThe Software Program may include internet links to other software applications and\/or internet web pages hosted and operated by third parties unaffiliated with Funai. You acknowledge and agree that Funai is not responsible in any way for the hosting, performance, operation, maintenance, or content of, such software applications and\/or internet web pages.\r\n\r\n\r\nLIMITATION OF REMEDY. TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, ANY AND ALL LIABILITY OF FUNAI UNDER THIS LICENSE AGREEMENT IS EXPRESSLY LIMITED TO THE GREATER OF THE PRICE PAID FOR THE SOFTWARE PROGRAM AND FIVE U.S. DOLLARS (OR THE EQUIVALENT IN LOCAL CURRENCY). YOUR SOLE REMEDY AGAINST FUNAI IN ANY DISPUTE UNDER THIS LICENSE AGREEMENT SHALL BE TO SEEK TO RECOVER ONE OF THESE AMOUNTS, UPON PAYMENT OF WHICH FUNAI SHALL BE RELEASED AND DISCHARGED OF ALL FURTHER OBLIGATIONS AND LIABILITY TO YOU\r\n\r\n\r\nIN NO EVENT WILL FUNAI, ITS SUPPLIERS, SUBSIDIARIES, OR RESELLERS BE LIABLE FOR ANY SPECIAL, INCIDENTAL, INDIRECT, EXEMPLARY, PUNITIVE, OR CONSEQUENTIAL DAMAGES (INCLUDING BUT NOT LIMITED TO LOST PROFITS OR REVENUES, LOST SAVINGS, INTERRUPTION OF USE OR ANY LOSS OF, INACCURACY IN, OR DAMAGE TO, DATA OR RECORDS, FOR CLAIMS OF THIRD PARTIES, OR DAMAGE TO REAL OR TANGIBLE PROPERTY, FOR LOSS OF PRIVACY ARISING OUT OR IN ANY WAY RELATED TO THE USE OF OR INABILITY TO USE THE SOFTWARE PROGRAM, OR OTHERWISE IN CONNECTION WITH ANY PROVISION OF THIS SOFTWARE LICENCE AGREEMENT), REGARDLESS OF THE NATURE OF THE CLAIM, INCLUDING BUT NOT LIMITED TO BREACH OF WARRANTY OR CONTRACT, TORT (INCLUDING NEGLIGENCE OR STRICT LIABILITY), AND EVEN IF FUNAI, OR ITS SUPPLIERS, AFFILIATES, OR REMARKETERS HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES, OR FOR ANY CLAIM BY YOU BASED ON A THIRD-PARTY CLAIM, EXCEPT TO THE EXTENT THIS EXCLUSION OF DAMAGES IS DETERMINED LEGALLY INVALID. THE FOREGOING LIMITATIONS APPLY EVEN IF THE ABOVE-STATED REMEDIES FAIL OF THEIR ESSENTIAL PURPOSE.\r\n\r\n\r\nLICENSE GRANT. Funai grants you the following rights provided you comply with all terms and conditions of this License Agreement: Use. The term \"Use\" means storing, loading, installing, executing, or displaying the Software Program. If Funai has licensed the Software Program to you for concurrent use, you must limit the number of authorized users to the number specified in your agreement with Funai. You may not separate the components of the Software Program for use on more than one computer. You agree that you will not Use the Software Program, in whole or in part, in any manner that has the effect of overriding, modifying, eliminating, obscuring, altering or de-emphasizing the visual appearance of any trademark, trade name, trade dress or intellectual property notice that appears on any computer display screens normally generated by, or as a result of, the Software Program\r\n\r\n\r\nCopying. You may make one (1) copy of the Software Program solely for purposes of backup, archiving, or installation, provided the copy contains all of the original Software Program\'s proprietary notices. You may not copy the Software Program to any public or distributed network.\r\n\r\n\r\nReservation of Rights. The Software Program, including all fonts, is copyrighted and owned by Funai and\/or its suppliers. Funai reserves all rights not expressly granted to you in this License Agreement.\r\n\r\n\r\nFreeware. Notwithstanding the terms and conditions of this License Agreement, all or any portion of the Software Program that constitutes software provided under public license by third parties (\"Freeware\") is licensed to you subject to the terms and conditions of the software license agreement accompanying such Freeware, whether in the form of a discrete agreement, shrink-wrap license, or electronic license terms at the time of download or installation. Use of the Freeware by you shall be governed entirely by the terms and conditions of such license.\r\n\r\n\r\nTRANSFER. You may transfer the Software Program to another end-user. Any transfer must include all software components, media, printed materials, and this License Agreement and you may not retain copies of the Software Program or components thereof. The transfer may not be an indirect transfer, such as a consignment. Prior to the transfer, the end-user receiving the transferred Software Program must agree to all these License Agreement terms. Upon transfer of the Software Program, your license is automatically terminated. You may not rent, sublicense, or assign the Software Program except to the extent provided in this License Agreement\r\n\r\n\r\nUPGRADES. To Use a Software Program identified as an upgrade, you must first be licensed to the original Software Program identified by Funai as eligible for the upgrade. After upgrading, you may no longer use the original Software Program that formed the basis for your upgrade eligibility.\r\n\r\n\r\nLIMITATION ON REVERSE ENGINEERING. You may not alter, decrypt, reverse engineer, reverse assemble, reverse compile or otherwise translate the Software Program or assist or otherwise facilitate others to do so, except as and to the extent expressly permitted to do so by applicable law for the purposes of inter-operability, error correction, and security testing. If you have such statutory rights, you will notify Funai in writing of any intended reverse engineering, reverse assembly, or reverse compilation. You may not decrypt the Software Program unless necessary for the legitimate Use of the Software Program\r\n\r\n\r\nADDITIONAL SOFTWARE. This License Agreement applies to updates or supplements to the original Software Program provided by Funai unless Funai provides other terms along with the update or supplement\r\n\r\n\r\nTERM. This License Agreement is effective unless terminated or rejected. You may reject or terminate this license at any time by destroying all copies of the Software Program, together with all modifications, documentation, and merged portions in any form, or as otherwise described herein. Funai may terminate your license upon notice if you fail to comply with any of the terms of this License Agreement. Upon such termination, you agree to destroy all copies of the Software Program together with all modifications, documentation, and merged portions in any form.\r\n\r\n\r\nAPPLICABLE LAW. This License Agreement is governed by the laws of Japan. No choice of law rules in any jurisdiction shall apply. The UN Convention on Contracts for the International Sale of Goods shall not apply\r\n\r\n\r\nEXPORT RESTRICTIONS. You may not (a) acquire, ship, transfer, or export, directly or indirectly, the Software Program or any direct product therefrom, in violation of any applicable export laws or (b) permit the Software Program to be used for any purpose prohibited by such export laws, including, without limitation, nuclear, chemical, or biological weapons proliferation.\r\n\r\n\r\nAGREEMENT TO CONTRACT ELECTRONICALLY. You and Funai agree to form this License Agreement electronically. This means that when you click the \"Agree\" button on this page or use this Funai product, you acknowledge your agreement to these License Agreement terms and conditions and that you are doing so with the intent to \"sign\" a contract with Funai.\r\n\r\n\r\nCAPACITY AND AUTHORITY TO CONTRACT. You represent that you are of the legal age of majority in the place you sign this License Agreement and, if applicable, you are duly authorized by your employer or principal to enter into this contract\r\n\r\n\r\nENTIRE AGREEMENT. This License Agreement (including any addendum or amendment to this License Agreement that is included with the Software Program) is the entire agreement between you and Funai relating to the Software Program. Except as otherwise provided for herein, these terms and conditions supersede all prior or contemporaneous oral or written communications, proposals, and representations with respect to the Software Program or any other subject matter covered by this License Agreement (except to the extent such extraneous terms do not conflict with the terms of this License Agreement, any other written agreement signed by you and Funai relating to your Use of the Software Program). To the extent any Funai policies or programs for support services conflict with the terms of this License Agreement, the terms of this License Agreement shall control.',
      buttons: ['OK']
    });
      alert.present();
    }

    showDevicelist() {
    const alert = this.alertCtrl.create({
      title: 'Supported devices',
      message: this.supported_deviceList,
      buttons: ['OK']
    });
      alert.present();
    }

    setLang(val: any) {
      console.log(val);
      sessionStorage.setItem('lang', val);
      this.navCtrl.setRoot(this.navCtrl.getActive().component);


      this.currentCountry = this.langList.filter(c => {
        if (c.lgID == val) {
          return c;
        }
      });
      console.log(this.currentCountry[0].flag);
      this.countryClass = this.currentCountry[0].flag;
      sessionStorage.setItem('langClass', this.currentCountry[0].flag);
    }

    // tabVisibl()
    // {
    //   return false;
    // }

    subOpt(event)
    {
      this.menuBackBtn = false;

      if(event.srcElement.className == 'img-circle')
      {
        this.actTabClass = event.srcElement.parentElement.className;
      }
      else
      {
        this.actTabClass = event.srcElement.className;
      }

      if(this.actTabClass == 'Download')
      {
      this.Download = true;
        this.Manual = false;
        this.Connect = false;
        this.Drivers = false;
        this.Firmware = false;
        this.agreeTC = true;
      }
      else if(this.actTabClass == 'Connect')
      {
      this.Connect = true;
        this.Manual = false;
        this.Download = false;
        this.Drivers = false;
        this.Firmware = false;
        this.agreeTC = true;
      }
      else if(this.actTabClass == 'Manual')
      {
        this.Manual = true;
        this.Connect = false;
        this.Download = false;
        this.Drivers = false;
        this.Firmware = false;
        this.agreeTC = true;
      }
      else if(this.actTabClass == 'Drivers')
      {
        this.Manual = false;
        this.Connect = false;
        this.Download = false;
        this.Drivers = true;
        this.Firmware = false;
        this.agreeTC = false;
      }
      else if(this.actTabClass == 'Firmware')
      {
        this.Manual = false;
        this.Connect = false;
        this.Download = false;
        this.Drivers = false;
        this.Firmware = true;
        this.agreeTC = false;
      }
      // else{
      //   this.backAllBtn = false;
      // }


      if(this.Manual == true || this.Connect == true || this.Download == true || this.Drivers == true || this.Firmware == true)
      {
        this.all = false;
        this.backAllBtn = true;
      }
      else
      {
        this.all = true;
        this.backAllBtn = false;
      }

    }

    backAll()
    {
      this.all = true;

      this.Manual = false;
      this.Connect = false;
      this.Download = false;
      this.Drivers = false;
      this.Firmware = false;
      this.backAllBtn = false;
      this.downloadDriver = false;
      this.downloadFirmware = false;
      this.agreeTC = false;
      this.menuBackBtn = true;
    }

    getContent() {

  console.log(sessionStorage.getItem('lang'));
  this.productListProvider.getProducts('static_content?lang_id=' + this.lang )  .subscribe(success => {
      // return this.httpService.get('static_content?lang_id=' + this.lang ).then(
        // (success) => {
          if (success) {
            this.downloadH1 = success.downloads_drivers_firmwares_softwares_tab_headings;
            this.downloadH2 = success.downloads_firmware_update_tab_headings;
            this.downloadH3 = success.downloads_manuals_tab_headings;
            this.downloadH4 = success.downloads_connect_your_mobile_device_tab_headings;
            this.downloadH5 = success.downloads_warranty_safety_sheets_tab_download_warranty_safety_sheets_text;

            this.downloadSub1 = success.downloads_download_manual_tab_owners_manual;
            this.downloadSub2 = success.downloads_download_manual_tab_quickstart_guide;
            this.downloadSub3 = success.downloads_connect_your_device_tab_connect_your_android_mobile_device_to_the_printer_text;
            this.downloadSub4 = success.downloads_connect_your_device_tab_connect_your_apple_mobile_device_to_the_printer_text;
            this.downloadSub5 = success.downloads_connect_your_device_tab_learn_about_airprint_text;
            this.downloadSub6 = success.downloads_connect_your_device_tab_learn_about_google_cloud_print_text;
            this.downloadSub7 = success.downloads_warranty_safety_sheets_tab_download_warranty_safety_sheets_text;


          } else {
            console.log('Error in success');
          }
        });
    }


    onSubmit() {

      // var tokenId = localStorage.getItem('token_array');
      // var tokenId = this.navParams.get('token_array');

      console.log(localStorage.getItem('prod_num_id'));

this.productListProvider.getProducts('downloads.json?lang=1&numid='+localStorage.getItem('prod_num_id'))  .subscribe(success => {
      // return this.httpService.get('downloads.json?lang=1&numid='+localStorage.getItem('prod_num_id')).then(
      //   (success) => {
          console.log(success.model_number);
          this.selectedProduct = success.model_number;
          if (success) {
            // this.getFile_path =[];
            console.log(success.drivers);
            for (var i = 0; i < success.length; i++) {
                this.getFile_path.push({f_path : success.drivers[i].drivers, f_desc : success[i].description})
            }
            console.log(this.getFile_path);

            this.android_url = success.android_playstore_url;
            this.ios_url = success.ios_playstore_url;
            this.ownersManual = success.downloads[1].owners;
            this.quickstartGuide = success.downloads[1].quickstart;
            this.GoogleCloud = success.google_cloud_url;
            this.AirPrintUrl = success.about_print_url;
            this.WarrantySafety = success.downloads[1].warranty_safety_sheet;

            this.supported_deviceList = success.supported_device;

            // this.OSTypes.push(success.firmwares);
            // console.log(success.firmwares[1])

            // this.productPics.push({ title: success.model_info[tokenId].model_number, modelPic: success.model_info[tokenId].img });
          } else {
            console.log('Error in success');
          }
        });
    }


    getOsDetails() {
      // return this.httpService.get('drivers?lang_id=' + this.lang + '&numid='+localStorage.getItem('prod_num_id')).then(

      // return this.httpService.get('drivers?lang_id=1&numid='+localStorage.getItem('prod_num_id')).then(
      //   (success) => {
      this.productListProvider.getProducts('drivers?lang_id=1&numid='+localStorage.getItem('prod_num_id'))  .subscribe(success => {
          if (success) {
            console.log(success);
            for (var i = 0; i < success.length; i++) {
                this.OSTypes.push({title: success[i].os_name, osId: success[i].os_id});
            }
          } else {
            console.log('Error in success');
          }
        });
    }


    downloadDetails(myId, typeof_download) {
      // return this.httpService.get('version_types?download_type=drivers&status=1&lang_id=' + this.lang + '&id=' + myId + '&numid='+localStorage.getItem('prod_num_id')).then(

      // return this.httpService.get('version_types?download_type=drivers&status=1&lang_id=1&id=' + myId + '&numid='+localStorage.getItem('prod_num_id')).then(

      // return this.httpService.get('version_types?download_type=' + typeof_download + '&status=1&lang_id=' + sessionStorage.getItem("lang") + '&id=' + myId + '&numid='+localStorage.getItem('prod_num_id')).then(
      //   (success) => {
      this.productListProvider.getProducts('version_types?download_type=' + typeof_download + '&status=1&lang_id=' + sessionStorage.getItem("lang") + '&id=' + myId + '&numid='+localStorage.getItem('prod_num_id'))  .subscribe(success => {
          if (success) {
            this.getFile_path =[];
            for (var i = 0; i < success.length; i++) {
                this.downloadName = success[i].drivers;
                this.downloadTypes = success[i].description;

                this.os_name = success[i].os_name;
                this.description = success[i].description;
                this.version_name = success[i].version_name;
                this.download_type = typeof_download;

                // console.log(success[i].drivers.length);

// for (var j = 0; j < success[j].drivers.length; j++) {
//
//   console.log(success[j].drivers);
//
// }
                // if(success[i].drivers)
                // {
                //   success[i].drivers


                // if(success[i].drivers)
                // {
                  this.getFile_path.push({ f_path : success[i].drivers, f_desc : success[i].description});
                //   console.log(this.getFile_path);
                // }
                // else if(success[i].firmware)
                // {
                //   // this.getFile_path = success[i].firmware;
                //   this.getFile_path.push({ f_path : success[i].firmware});
                // }
                // else{
                //   // this.getFile_path = 'Path not available';
                //   this.getFile_path.push({ f_path : 'Path not available'});
                // }


                // console.log('drivers - ' + success[i].drivers);
                // console.log('this.getFile_path is - ' + this.getFile_path.length);
            }

            // console.log('this.getFile_path is - ' + this.getFile_path[0]);
          } else {
            console.log('Error in success');
          }
        });
    }

    // http://124.30.44.230/funaihelpver1/api/ver1/drivers?numid=1&lang_id=1

    productBanner() {

      var tokenId = localStorage.getItem('token_array');
      // var tokenId = this.navParams.get('token_array');
      // console.log(tokenId);

      this.productListProvider.getProducts('productlist.json?compid=1&prodid=1&lang=1')  .subscribe(success => {
      // return this.httpService.get('productlist.json?compid=1&prodid=1&lang=1').then(
      //   (success) => {
          if (success) {
            this.bannerPics.push({ title: success.model_info[tokenId].model_number, modelPic: success.model_info[tokenId].img });
          } else {
            console.log('Error in success');
          }
        })
    }

    gotoTrob()
    {
      this.navCtrl.push(TroubleshootPage);
    }
    pushHome() {
      // this.navCtrl.push(HomePage);
      this.navCtrl.setRoot(HomePage);
    }

    gotoBack()
    {
      this.navCtrl.pop();
    }
    pushRegister() {
       this.navCtrl.setRoot(RegisterPage);
    }


  }
