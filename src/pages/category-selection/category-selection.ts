// import { Component } from '@angular/core';
// import { IonicPage, NavController, NavParams } from 'ionic-angular';
// import { NgModule } from '@angular/core';
// import { IonicPageModule } from 'ionic-angular';
import { Component, OnInit } from '@angular/core';
import { NavController } from 'ionic-angular';

// import { Component, OnInit, ViewChild } from '@angular/core';
// import { Nav, NavController, NavParams } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import 'rxjs/add/operator/switchMap';
// import { Headers, RequestOptions } from '@angular/http';
// import { HttpService } from '../../services/http.service';
import { RegisterPage } from './../register/register';
import { HomePage } from './../home/home';
// import { Location } from '@angular/common';
import { ProductListProvider } from '../../providers/product-list/product-list';

import { DownloadPage} from './../download/download';
import { TroubleshootPage} from './../troubleshoot/troubleshoot'

// @IonicPage()
@Component({
  selector: 'page-category-selection',
  templateUrl: 'category-selection.html',
})
export class CategorySelectionPage implements OnInit {

  data: any;
  countryName: any;
  currentCountry;
  countryClass = sessionStorage.getItem('langClass');

  modelPic: any;

  constructor(
    // private _HttpService: HttpService,
    // private httpService: HttpService,
    public navCtrl: NavController,
    private productListProvider:ProductListProvider,
    // public navParams: NavParams
    // private router: Router
  ) { }

    ionViewWillEnter(){
      // var tokenId = localStorage.getItem('token_array');

      this.modelPic = localStorage.getItem('bnrImg');
// console.log(this.modelPic);

      // this.productListProvider.getProducts('productlist.json?compid=1&prodid=1&lang=1')  .subscribe(success => {
      //     console.log(success);
      //     if (success) {
      //       this.modelPic = success.model_info[tokenId].img;
      //     }
      //
      //   });

    }

  vals: any;

  tabsPage = TabsPage;

  // @ViewChild(Nav) nav: Nav;

  rootPage: any;

  // productPics: Array<{ title: string, modelPic: string }>;
  productPics: Array<{ modelPic: string }>;

  productH1: string;
  productH2: string;

  productTxt1: string;
  productTxt2: string;

  lang: any;


  langList: Array<{ lgName: string, lgID: number, flag: string }>;

  menuBackBtn: Boolean = true;

  ngOnInit(): void {
    this.vals = '';
    // this.onSubmit();
    this.productPics = [];

    console.log()


    this.langList = [{ lgName: 'United States (English)', lgID: 1, flag: 'us' },
    { lgName: 'Canada (French)', lgID: 109, flag: 'canada' },
    { lgName: 'United States (Spanish)', lgID: 111, flag: 'us' },
    { lgName: 'Canada (English)', lgID: 110, flag: 'canada' },
    { lgName: 'United Kingdom (English)', lgID: 108, flag: 'uk' }];


    if (sessionStorage.getItem("lang") == null && sessionStorage.getItem("lang") == "") {
      this.lang = 1;
      this.countryName = 1;
      sessionStorage.setItem('lang', '1');
    }
    else {
      this.lang = sessionStorage.getItem('lang');
      this.countryName = sessionStorage.getItem('lang');
      console.log(this.countryName);
    }

    this.getContent();

  }

  selectOptions: any = {
    title: 'Language',
    // subTitle: 'Select your toppings',
    mode: 'md',
    // cssClass: 'my-class'
  };

  doRefresh(refresher) {
    console.log('Begin async operation', refresher);
    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }


  setLang(val: any) {
    //console.log(val);
    sessionStorage.setItem('lang', val);
    //sessionStorage.setItem('langClass', val);
    this.navCtrl.push(this.navCtrl.getActive().component);

    this.currentCountry = this.langList.filter(c => {
      if (c.lgID == val) {
        return c;
      }
    });
    //console.log(this.currentCountry[0].flag);
    this.countryClass = this.currentCountry[0].flag;
    sessionStorage.setItem('langClass', this.currentCountry[0].flag);
    console.log(this.countryClass);
  }

  getContent() {

    this.productListProvider.getProducts('static_content?lang_id=' + this.lang)  .subscribe(success => {
        if (success) {
          this.productH1 = success.product_downloads_heading;
          this.productH2 = success.product_resolution_center_heading;

          this.productTxt1 = success.product_help_message_text;
          this.productTxt2 = success.resolution_product_help_message_text;
        }

      });


  }


  // goAnOtherPage(tokenNumber) {
  //   this.navCtrl.setRoot(TabsPage);
  //   this.navCtrl.setRoot(TabsPage, { 'token_array': tokenNumber });
  // }


  ionViewDidLoad() {
    //console.log('ionViewDidLoad CategorySelectionPage');
  }

  backButton() {
    this.navCtrl.setRoot(CategorySelectionPage);
  }
  testit() {
    alert('007');
  }

  gotTabPage(tabValue) {
    if (tabValue == 0) {
      localStorage.setItem('activeTab', tabValue);
      this.navCtrl.push(DownloadPage);
      console.log(tabValue);
    } else {
      localStorage.setItem('activeTab', tabValue);
      this.navCtrl.push(TroubleshootPage);
      console.log(tabValue);
    }
  }


  pushRegister() {
     this.navCtrl.setRoot(RegisterPage);
  }
  pushBack() {
    // this.navCtrl.setRoot(HomePage);
    this.navCtrl.pop();
  }
  pushHome() {
    this.navCtrl.setRoot(HomePage);
  }
  gotoBack()
  {
    this.navCtrl.pop();
  }

}
