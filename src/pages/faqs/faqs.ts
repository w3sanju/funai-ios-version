import { Component } from '@angular/core';
import { NavController, NavParams, Platform } from 'ionic-angular';
import { ProductListProvider } from '../../providers/product-list/product-list';
// import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { FormGroup, FormControl } from '@angular/forms';
import * as $ from "jquery";
// declare var jquery:any;
declare var $ :any;
import { AlertController } from 'ionic-angular';

@Component({
    selector: 'page-faqs',
    templateUrl: 'faqs.html',
})

export class FaqsPage {

  levelValue = localStorage.getItem('levelTo5Value');

  productListing:any;

  surveyForm: FormGroup;
  mySurvey: any;
  mySurveyChildOne: any;
  survey:any;

  feedbackoverlayHidden: boolean = true;
  feedbackFormoverlayHidden: boolean = true;
  thankYouMsgHidden: boolean = true;

  productNameId = localStorage.getItem('prod_num_id');

  deviceIs: any;
  data: any;
  tabVisibl: any;
  all: any;

  show: any;
  showSib: any;


    faqs :any;


    showAlertMessage: boolean = true;
shouldLeave: boolean = false;

lang: any;
langList: Array<{ lgName: string, lgID: number, flag: string }>;

public langName: string = "US spanish";

  countryName: any;
  currentCountry;
  countryClass: any;


  selectOptions: any = {
    title: 'Language',
    mode: 'md',
    cssClass: 'my-class'
  };

    constructor(
        private productListProvider:ProductListProvider,
        public navCtrl: NavController,
        public navParams: NavParams,
        public plt: Platform,
        public alertCtrl: AlertController
    ){

    }

    ngOnInit(){
      this.platform();
        this.faqsData();
        this.productList();


        if (!localStorage.getItem('lang')) {
          // sessionStorage.setItem('lang', '1');
          console.log('no lang');
          this.countryClass = 'us';
        }
        else {
          console.log('got lang');
        }


        this.langList = [{ lgName: 'United States (English)', lgID: 1, flag: 'us' },
            { lgName: 'Canada (French)', lgID: 109, flag: 'canada' },
            { lgName: 'United States (Spanish)', lgID: 111, flag: 'us' },
            { lgName: 'Canada (English)', lgID: 110, flag: 'canada' },
            { lgName: 'United Kingdom (English)', lgID: 108, flag: 'uk' }];

        if (sessionStorage.getItem("lang") !== null && sessionStorage.getItem("lang") !== "") {
          this.lang = sessionStorage.getItem('lang');
          this.countryName = sessionStorage.getItem('lang');
          this.countryClass = sessionStorage.getItem('langClass');
          console.log(this.countryName);
        }
        else {
          this.lang = sessionStorage.setItem('lang', '1');
          this.countryClass = 'us';
          sessionStorage.setItem('langClass','us');
          this.countryName = 1;
          console.log(this.countryName);
        }

    }

    ionViewCanLeave()
    {
      // this.feedbackFormOpen();
      // return false;

      this.feedbackoverlayHidden = false;
      return this.shouldLeave;

    }

    // ionViewCanLeave() {
    //   // alert('Exit from page');
    // }

    faqsData(){

          this.productListProvider.getProducts('faqs.json?num_id='+localStorage.getItem('prod_num_id')+'&thirdlevel_id='+localStorage.getItem('level_id3New')+'&lang=1')  .subscribe(success => {

            this.faqs = success.faqs.split('src="/media/').join('src="http://124.30.44.230/funaihelpver1//media/').split('<div><a href="#" onclick="window.close(); return false;">CLOSE</a></div>').join('');

          });
    }
    toggleSection(i) {
        this.faqs[i].open = !this.faqs[i].open;
    }
    toggleItem(i, j) {
        this.faqs[i].children[j].open = !this.faqs[i].children[j].open;
    }


    platform(){
      if (this.plt.is('iphone')) {
        this.deviceIs = 'iphone';
      }
      else if(this.plt.is('android'))
      {
        this.deviceIs = 'Android';

      }
    }

    feedbackFormOpen(){
      this.feedbackFormoverlayHidden = false;
    }

    yesThankYou(){
      // this.feedbackoverlayHidden = true;
      this.feedbackoverlayHidden = false;
      this.thankYouMsgHidden = true;
      this.navCtrl.pop();
      this.shouldLeave = true;
    }

    form = new FormGroup({
      survey: new FormControl(),
      mySurveyChildOne: new FormControl(),
    });

    putSurvey(){
      // var data = this.form.value;
      var postdata = {
        channel: this.mySurveyChildOne,
        feedback_id: this.mySurvey,
        num_id: localStorage.getItem('prod_num_id'),
        third_level:'',
        page_name: "Troubleshooting",
        device_id: this.deviceIs,
      }
      console.log(postdata);
      this.shouldLeave = true;

      $.ajax({
        url: 'http://124.30.44.230/funaihelpver1/api/ver1/survey',
        method:'POST',
        crossDomain: true,
        beforeSend:function(xhr){
          xhr.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
        },
        async:true,
        data:postdata,
        success: function(result) {
          console.log(result);
          $('.my-overlay').hide();
          // this.justLeave();
        },
        error: function(request,msg,error) {
        }
      });
      this.navCtrl.pop();

    }

    justLeave()
    {
      console.log('justLeave called now');
      this.navCtrl.pop();
    }

    productList(){
      this.productListProvider.getProducts('product.json?productid=1') .subscribe(success => {
        this.productListing = success;
        console.log(this.productListing);
      });
    }

    gotoBack()
    {
      this.navCtrl.pop();
    }

    setLang(val: any) {
    console.log(val);
    sessionStorage.setItem('lang', val);
    localStorage.setItem('lang', val);

    // this.navCtrl.setRoot(HomePage);

    this.currentCountry = this.langList.filter(c => {
      if (c.lgID == val) {
        return c;
      }
    });
    console.log(this.currentCountry[0].flag);
    this.countryClass = this.currentCountry[0].flag;
    sessionStorage.setItem('langClass', this.currentCountry[0].flag);
    //this.navCtrl.push(this.navCtrl.getActive().component);
  }

}
