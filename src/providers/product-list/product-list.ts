import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
// import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';

@Injectable()
export class ProductListProvider {
  apiKey = '79b24bf4288affd5';
  url;

  // private options: RequestOptions;

  constructor(
    public http: Http,
  ) {
    console.log('Hello WeatherProvider Provider');
    // this.url = 'https://api.wunderground.com/api/' + this.apiKey+ '/conditions/q';
    this.url = 'http://124.30.44.230/funaihelpver1/api/ver1/';
  }

  // private configure() {
  //     let token = localStorage.getItem('token');
  //     let headers = new Headers();
  //     // console.log(token);
  //     if(token !== null){
  //         headers = new Headers({
  //             'Content-Type': 'application/x-www-form-urlencoded',
  //             'Token': token
  //         });
  //     }else{
  //         headers = new Headers({
  //             'Content-Type': 'application/x-www-form-urlencoded',
  //         });
  //     }
  //
  //     let options = new RequestOptions({ headers: headers });
  // }

  getWeather(city,state) {
    return this.http.get(this.url)
    .map(res => res.json());
  }

  getProducts(link) {
    return this.http.get(this.url + link)
    .map(res => res.json());
  }

  postProducts(link, Mydata) {
    return this.http.post(this.url + link, Mydata)
    .map(res => res.json());
  }

  putProducts(link, Mydata) {

    let token = localStorage.getItem('token');
    let headers = new Headers();
    // console.log(token);
    if(token !== null){
        headers = new Headers({
            'Content-Type': 'application/x-www-form-urlencoded',
            'Token': token
        });
    }else{
        headers = new Headers({
            'Content-Type': 'application/x-www-form-urlencoded',
        });
    }

    let options = new RequestOptions({ headers: headers });

    return this.http.put(this.url + link, Mydata, options)
    .map(res => res.json());
  }



}
